import config
import json

categories = {}
subjects = {}
msgs = []

def init():
    global subjects
    global categories
    global msgs
    with open(config.board_cfg) as cfg:
        data = json.load(cfg)
        subjects = data['subjects']
        categories['sfw'] = data['sfw']
        categories['nsfw'] = data['nsfw']
        categories['hobby'] = data['hobby']
        msgs = data ['msgs']



def exists(board):
    for cat in categories:
        if board in categories[cat]:
            return True
    else:
        return False

def subject(board):
    try:
        return subjects[board]
    except KeyError: 
        return 'I really dont even know how this happened'
    
def all_boards():
    return [i for i in subjects]
