from flask import Flask
from flask import request
from flask import render_template, url_for
from flask import jsonify
import random, argparse

import boards, config, api
from api import BoardResponse

app = Flask('Meme router for muh chan', template_folder=config.templates)

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html', 
        comfy_msg=random.choice(boards.msgs),
        sfw=boards.categories['sfw'], 
        nsfw=boards.categories['nsfw'],
        hobby=boards.categories['hobby'],
        stylesheet='index.css',
        pagetitle='comfychan')

@app.route('/<board>', methods=['GET'])
def board(board):
    '''
    TODO: add support for this endpoint to post new threads
    Here we are getting a list of thread objects to populate the view with
    NOTE: the return by the api should be ready for the frontend so basically packaged as json
    '''
    _name=''
    _subject=''

    if boards.exists(board):
        _name = board
        _subject = boards.subject(board)
    else:
        _name = 'Not Found'
        _subject = 'The board requested was not found'
    
    return render_template('board.html', 
        stylesheet = 'board.css',
        pagetitle =_name,
        boardsubject = _subject,
        boardslist = boards.all_boards())
    

@app.route('/<board>/<thread>', methods=['GET'])
def board_thread(board, thread):
    if boards.exists(board): #and threads.alive(thread):
        return render_template('thread.html',
            stylesheet='thread.css',
            boardslist = boards.all_boards())

    return render_template('nothread.html')

@app.route('/api/threads/<board>', methods=['GET', 'POST'])
def get_threads(board):
    if not boards.exists(board):
        # don't bother connecting to db since we already know the response is uselsess
        api_response = BoardResponse(board, exist=False)
        return api_response.content, 400
    else:
        api_response = BoardResponse(board)
        return api_response.content, 200


if __name__ == '__main__':
    cli_parser = argparse.ArgumentParser(description='Flask app for comfychan')
    cli_parser.add_argument('-d','--debug', help='Sets config for development', 
            required=False, action='store_true')

    cli_parser.add_argument('-p','--production', help='Sets config for production', 
        required=False, action='store_true')

    args = vars(cli_parser.parse_args())

    # Dealing with arguments here
    if args['debug']:
        print('Lanuching development app')
        config.DEBUG=True

    if args['production']:
        print('Lanuching production')
        config.DEBUG=False

    # safety check for failed env setup
    if config.DEBUG == None:
        exit(1)

    if config.DB_HOST == None:
        exit(1)

    boards.init()
    app.run(debug=config.DEBUG)
