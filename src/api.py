from config import DB_HOST, DB_PASS, DB_PORT, DEBUG
from flask import jsonify
from flask.json import JSONEncoder
import time
import redis



class Post(): 
    def __init__(self, parent, title, body, poster):
        # these should basically be all strings i think
        self.parent = parent
        self.title = title
        self.body = body
        self.poster = poster


class BoardResponse(object):
    def __init__(self, board, exist=True):
        self.board = board
        self.content = jsonify([])

        try:
            if exist:
                self.db_conn = redis.StrictRedis(host=DB_HOST, port=DB_PORT, password=DB_PASS)
                self.content = self.set_threads()

            else:   
                self.content = self.empty_response()

        except Exception as e:
            print(f'Connection failed for reason: {e}')
            self.db_conn = None


    def set_threads(self):
        # Look for a `thread` belonging to `board`
        try:
            threads = []
            for key in self.db_conn.scan_iter("post_*"):
                item = self.db_conn.hgetall(key.decode('utf-8'))

                poster = item[b'poster'].decode('utf-8')
                title = item[b'title'].decode('utf-8')
                body = item[b'body'].decode('utf-8')
                _ = {'title':title, 'body': body, 'poster':poster}
                threads.append(_)

            return jsonify(threads)

        except AttributeError as e:
            # connection probably failed here
            print(e)
            return self.empty_response()


    def empty_response(self):
        return jsonify([Post(
            parent=self.board,
            poster= "shockrah",
            title= f'{self.board} has no threads up right now',
            body= "Something's probably broken :(",
        ).__dict__])

