import os
templates = os.path.abspath('templates')

board_cfg = 'src/config.json' # relative to the run script

DEBUG= None

DB_HOST = os.getenv('DB_HOST')
DB_PASS = os.getenv('DB_PASS')
DB_PORT = os.getenv('DB_PORT')

REDIS_URL = os.getenv('DB_URL')
