var board = document.title;
var endpoint = '/api/threads/' + board;
var response;

function elTemplate(_class) {
	// TODO: allow for the passing of kv pairs for easier dynamic bindings
	let ret = document.createElement('div');
	ret.className = _class;
	return ret;
}
function makeThread(title, poster, body) {
	let box = elTemplate('thread');

	let boxTitle = elTemplate('thread-title');
	let titleLink = document.createElement('a'); 
	let _ = document.createTextNode(title); 
		titleLink.appendChild(_);
		boxTitle.appendChild(titleLink); 

	box.appendChild(boxTitle); // making the title a link

	if(poster != null) {
		let p = elTemplate('thread-poster');
		let pText = document.createTextNode(poster);
		p.appendChild(pText);
		box.appendChild(p);
	}
	return box;
}
function build_content(data) {
	let container = document.getElementById('threads-container');
	let target = document.getElementById('row0');
	for(let i = 0;i < data.length;i++) {
		if(i % 5 == 0 && i != 0) {
			let newRow = elTemplate('thread-container');
			newRow.id = 'row' + i;
			container.appendChild(newRow);
			console.log(newRow);
			target = document.getElementById('row'+i);
		}
		let thread = makeThread(data[i]['title'], data[i]['poster'], data[i]['body']);
		target.appendChild(thread);
	}
}

$(document).ready(function() {
	var request = $.ajax({
		method: 'GET',
		url: '/api/threads/' + document.title,
		dataType: 'json'
	});

	request.done(function(data) {
		build_content(data);
	});
});
